# IMPORTS
import os
from shutil import get_terminal_size
# FUNCTIONS
# clear terminal
def clear():
   if os.name == "posix":
       # Unix/Linux/MacOS/BSD/etc
       os.system('clear')
   elif os.name in ("nt", "dos", "ce"):
       # DOS/Windows
       os.system('CLS')
   else:
       # fallback for other oss. OSs? Operating systems.
       print("\n" * get_terminal_size().lines, end='')
# print by line
def pbl(document):
   for lines in document:
       print(lines.strip())
# MAIN
# take input
filePath = input("Path to text: ")
clear()
# open file in read/write if the file exists
try:
   file = open(filePath, "r+")
# if the file doesn't exist:
except FileNotFoundError:
# create a file with 10 lines, each saying "line"
   file = open(filePath, "x") # "x" opens the file in create\write mode ...file.writelines("line\nline\nline\nline\nline\nline\nline\nline\nline\nline")
# close the file, reopen it in read/write mode
   file.close()
   file = open(filePath, "r+")
doc = file.readlines()
# editing loop
while True:
   # print current document status
   pbl(doc)
   # take current line to work on
   line = input()
   # detect if previous input was null
   if line == "":
       # erase the file's data
       file.seek(0)
       file.truncate()
       # save the current state of the document to the file
       file.writelines(doc)
       # wait for input, then close the file and program
       file.close()
       quit("All done!")
   # save the edit to the variable, then clear the screen
   edit = input()
   # save the edit, unless it is empty, if so delete the line
   if edit != "":
       doc[int(line)-1] = edit+"\n"
   elif edit == "":
       del doc[int(line)-1]
   clear()
